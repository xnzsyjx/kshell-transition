use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

use nalgebra::DMatrix;

use crate::tool::BasicForUInt;

pub struct Orbit {
    pub n: u16,
    pub l: u16,
    pub j2: u16,
    pub tz2: i16,
}

// #[allow(dead_code)]
pub struct Operator {
    name: String, // name of operator

    // model space
    orbits: HashMap<u16, Orbit>,
    n_orb: u16,    // number of orbitals
    tbc_jmax: u16, // maximum angular momentum of 2 body state |ab>, it should be >= 1 at least
    tbc: HashMap<(u16, u16), u16>,
    n_tbc: u16,

    rank_j: u16,
    rank_t: i16,
    parity: u16, // 0 for +1, 1 for -1

    zerobody: f64,
    onebody: HashMap<u16, f64>,
    twobody: HashMap<(u16, u16), DMatrix<f64>>,
}

impl Operator {
    // Constructor for operator
    pub fn new_op(onebfile: &str, twobfile: &str) -> Self {
        let (name, orbits, n_orb, tbc_jmax, rank_j, rank_t, parity, zerobody, onebody): (
            String,
            HashMap<u16, Orbit>,
            u16,
            u16,
            u16,
            i16,
            u16,
            f64,
            HashMap<u16, f64>,
        ) = Operator::read_1bfile(onebfile);
        let mut tbc = HashMap::new();
        let mut index = 0;
        let n_tbc = n_orb * n_orb;
        for i in 1..=n_orb {
            for j in i..=n_orb {
                tbc.insert((i, j), index);
                index += 1;
            }
        }
        let mut op = Operator {
            name: name.clone(),
            orbits,
            n_orb,
            tbc_jmax,
            tbc,
            n_tbc,
            rank_j,
            rank_t,
            parity,
            zerobody,
            onebody,
            twobody: HashMap::new(),
        };
        op.read_2bfile(twobfile);
        op
    }
    // read 1b.op file for new_op constructor
    // the return is a tuple of (name, orbits, n_orb, tbc_jmax, rank_j, rank_t, parity, zerobody, onebody)
    fn read_1bfile(
        onebfile: &str,
    ) -> (
        String,
        HashMap<u16, Orbit>,
        u16,
        u16,
        u16,
        i16,
        u16,
        f64,
        HashMap<u16, f64>,
    ) {
        let mut obme: HashMap<u16, f64> = HashMap::new();
        let file = File::open(onebfile);
        let all_lines: Vec<String> =
            BufReader::new(file.expect("Trouble reading 1b.op file exiting."))
                .lines()
                .map(|line| line.unwrap().trim().to_string())
                .collect();
        let name = all_lines
            .get(0)
            .unwrap()
            .split_once("generated")
            .unwrap()
            .0
            .split_whitespace()
            .last()
            .unwrap()
            .to_string();
        let rank_j = all_lines
            .get(1)
            .unwrap()
            .split_whitespace()
            .last()
            .unwrap()
            .parse::<u16>()
            .unwrap();
        let rank_t = all_lines
            .get(2)
            .unwrap()
            .split_whitespace()
            .last()
            .unwrap()
            .parse::<i16>()
            .unwrap();
        let parity = if all_lines.get(3).unwrap().contains("+1") {
            0
        } else {
            1
        };
        let zerobody = all_lines
            .get(4)
            .unwrap()
            .split_whitespace()
            .last()
            .unwrap()
            .parse::<f64>()
            .unwrap();
        let mut orbits = HashMap::new();
        let mut n_orb = 0;
        let mut tbc_jmax = 1;
        for i in 6..all_lines.len() {
            let l = all_lines.get(i).unwrap().trim();
            if l == "!" {
                break;
            }
            let line: Vec<&str> = l.split_whitespace().collect();
            let idx = line.get(1).unwrap().parse::<u16>().unwrap();
            let n = line.get(2).unwrap().parse::<u16>().unwrap();
            let l = line.get(3).unwrap().parse::<u16>().unwrap();
            let j2 = line.get(4).unwrap().parse::<u16>().unwrap();
            let tz2 = line.get(5).unwrap().parse::<i16>().unwrap();
            if idx > n_orb {
                n_orb = idx;
            }
            if j2 > tbc_jmax {
                tbc_jmax = j2;
            }
            orbits.insert(idx, Orbit { n, l, j2, tz2 });
        }
        let begin_line = 9 + n_orb as usize;
        for i in begin_line..all_lines.len() {
            let line: Vec<&str> = all_lines.get(i).unwrap().split_whitespace().collect();
            let a: u16 = line.get(0).unwrap().parse().unwrap();
            let b: u16 = line.get(1).unwrap().parse().unwrap();
            let me: f64 = line.get(2).unwrap().parse().unwrap();
            obme.insert(a * 10 + b, me);
        }
        return (
            name, orbits, n_orb, tbc_jmax, rank_j, rank_t, parity, zerobody, obme,
        );
    }
    fn read_2bfile(&mut self, twobfile: &str) -> () {
        self.twobody = HashMap::new();
        let file = File::open(twobfile);
        let all_lines: Vec<String> =
            BufReader::new(file.expect("Trouble reading 2b.op file exiting."))
                .lines()
                .map(|line| line.unwrap().trim().to_string())
                .collect();
        if self.name
            != all_lines
                .get(0)
                .unwrap()
                .split_once("generated")
                .unwrap()
                .0
                .split_whitespace()
                .last()
                .unwrap()
                .to_string()
            || self.rank_j
                != all_lines
                    .get(1)
                    .unwrap()
                    .split_whitespace()
                    .last()
                    .unwrap()
                    .parse::<u16>()
                    .unwrap()
            || self.rank_t
                != all_lines
                    .get(2)
                    .unwrap()
                    .split_whitespace()
                    .last()
                    .unwrap()
                    .parse::<i16>()
                    .unwrap()
            || self.parity
                != (if all_lines.get(3).unwrap().contains("+1") {
                    0
                } else {
                    1
                })
            || (self.zerobody
                - all_lines
                    .get(4)
                    .unwrap()
                    .split_whitespace()
                    .last()
                    .unwrap()
                    .parse::<f64>()
                    .unwrap())
                > f64::EPSILON
        {
            panic!("Operator parameters and 2b.op file do not match!");
        }
        for i in 0..=self.tbc_jmax {
            let jab = i;
            for dj in 0..=self.rank_j {
                let jcd = jab + dj;
                if jcd > self.tbc_jmax {
                    continue;
                }
                self.twobody.insert(
                    (jab, jcd),
                    DMatrix::zeros(self.n_tbc as usize, self.n_tbc as usize),
                );
                // println!("{} {}", jab, jcd);
            }
        }
        let begin_line = 8 + self.n_orb as usize;
        for i in begin_line..all_lines.len() {
            let line: Vec<&str> = all_lines
                .get(i)
                .unwrap()
                .trim()
                .split_whitespace()
                .collect();
            let a: u16 = line.get(0).unwrap().parse().unwrap();
            let b: u16 = line.get(1).unwrap().parse().unwrap();
            let c: u16 = line.get(2).unwrap().parse().unwrap();
            let d: u16 = line.get(3).unwrap().parse().unwrap();
            let jab: u16 = line.get(4).unwrap().parse().unwrap();
            let jcd: u16 = line.get(5).unwrap().parse().unwrap();
            let me: f64 = line.get(6).unwrap().parse().unwrap();
            let mut phase = 1.0;
            let bra = if a > b {
                phase *= ((self.get_orb(a).j2 + self.get_orb(b).j2) / 2 + 1 - jab).phase();
                self.get_tbc_index(b, a)
            } else {
                self.get_tbc_index(a, b)
            } as usize;
            let ket = if c > d {
                phase *= ((self.get_orb(c).j2 + self.get_orb(d).j2) / 2 + 1 - jcd).phase();
                self.get_tbc_index(d, c)
            } else {
                self.get_tbc_index(c, d)
            } as usize;
            *self
                .twobody
                .get_mut(&(jab, jcd))
                .unwrap()
                .index_mut((bra, ket)) = me * phase;
        }
    }
    #[inline(always)]
    pub fn get_name(&self) -> String {
        self.name.clone()
    }
    #[inline(always)]
    pub fn get_orb(&self, n: u16) -> &Orbit {
        self.orbits.get(&n).unwrap()
    }
    #[inline(always)]
    pub fn get_oi_j2(&self, i: u16) -> f64 {
        self.orbits.get(&i).unwrap().j2 as f64
    }
    #[inline(always)]
    pub fn get_tbc_index(&self, i: u16, j: u16) -> u16 {
        *self.tbc.get(&(i, j)).unwrap()
    }
    #[inline(always)]
    pub fn get_rank_j(&self) -> u16 {
        self.rank_j
    }
    #[inline(always)]
    pub fn is_scalar(&self) -> bool {
        self.rank_j == 0
    }
    #[inline(always)]
    pub fn get_zerobody(&self) -> f64 {
        self.zerobody
    }
    /// get matrix element
    #[inline(always)]
    pub fn get_obme(&self, i: u16, j: u16) -> f64 {
        *self.onebody.get(&(i * 10 + j)).unwrap_or(&0.0)
    }
    pub fn get_tbme(&self, j_ij: u16, j_kl: u16, i: u16, j: u16, k: u16, l: u16) -> f64 {
        let bra = self.get_tbc_index(i, j) as usize;
        let ket = self.get_tbc_index(k, l) as usize;
        match j_ij > j_kl {
            true => {
                self.twobody.get(&(j_kl, j_ij)).unwrap().index((ket, bra)) * (j_ij + j_kl).phase()
            }
            _ => *self.twobody.get(&(j_ij, j_kl)).unwrap().index((bra, ket)),
        }
    }
}
