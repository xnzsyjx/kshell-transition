use em::{
    electric_quadrupole_moment, electro, em_factor, get_tr_energy, magnetic, magnetic_dipole_moment,
};
use redius::{get_n_redius, get_p_redius};

use self::em::EM;

static OPNAME_LIST: [&str; 14] = [
    "E1", "E2", "E3", "E4", "E5", "E6", "M1", "M2", "M3", "M4", "M5", "M6", "Rp2", "Rn2",
];

fn find_op_index(op: &str) -> Option<usize> {
    OPNAME_LIST.iter().position(|&x| x == op)
}

pub fn calculate_expection(
    i_num: u8,
    i_j2: f64,
    f_num: u8,
    f_j2: f64,
    opname: String,
    zerobody: f64,
    one_plus_two: f64,
    transition_density_file: String,
) {
    match find_op_index(&opname) {
        Some(index) => {
            if (index == 1 || index == 6) && i_num == f_num {
                if index == 1 {
                    electric_quadrupole_moment(i_j2, zerobody, one_plus_two); // electric quadrupole moment
                } else {
                    magnetic_dipole_moment(i_j2, zerobody, one_plus_two); // magnetic dipole moment
                }
                return;
            }
            match index {
                0..=5 => {
                    let (tr_energy, a) = get_tr_energy(i_num, f_num, transition_density_file);
                    let electro_head = em_factor(tr_energy, index + 1, EM::E);
                    electro(
                        index + 1,
                        i_j2,
                        f_j2,
                        zerobody,
                        one_plus_two,
                        electro_head,
                        a,
                    )
                } // E1 to E6
                6..=11 => {
                    let (tr_energy, a) = get_tr_energy(i_num, f_num, transition_density_file);
                    let magnetic_head = em_factor(tr_energy, index - 5, EM::M);
                    magnetic(
                        index - 5,
                        i_j2,
                        f_j2,
                        zerobody,
                        one_plus_two,
                        magnetic_head,
                        a,
                    )
                } // M1 to M6
                12 => get_p_redius(i_j2, f_j2, zerobody, one_plus_two, transition_density_file), // Rp2
                13 => get_n_redius(i_j2, f_j2, zerobody, one_plus_two), // Rn2
                _ => panic!("Wrong operator name!!!"),
            }
        }
        None => println!("Don't have to calculate expection value.\nEnd of program."),
    }
}

mod em {
    use std::{
        fs::File,
        io::{BufRead, BufReader},
    };

    use crate::tool::{constant::{ALPHA, C, HBARC, M_PROTON, PI}, get_az};

    pub enum EM {
        E,
        M,
    }

    pub fn electric_quadrupole_moment(j2: f64, zerobody: f64, one_plus_two: f64) {
        if (j2 - 1.0) <= f64::EPSILON {
            println!("This state has not electric quadrupole moment");
            println!("<Q> = 0.0");
            return;
        }
        println!(
            "<Q> = {}",
            (3.2 * PI * j2 * (j2 - 1.0) / ((j2 + 2.0) * (j2 + 1.0) * (j2 + 3.0))).sqrt() * zerobody
                + one_plus_two / (j2 + 1.0)
        );
    }
    pub fn magnetic_dipole_moment(j2: f64, zerobody: f64, one_plus_two: f64) {
        if j2 <= f64::EPSILON {
            println!("This state has not magnetic dipole moment");
            println!("<M> = 0.0");
            return;
        }
        println!(
            "<M> = {}",
            (4.0 * PI * j2 / ((j2 + 2.0) * (j2 + 1.0) * 3.0)).sqrt() * zerobody
                + one_plus_two / (j2 + 1.0)
        );
    }
    pub fn electro(
        l: usize,
        i_j2: f64,
        f_j2: f64,
        zerobody: f64,
        one_plus_two: f64,
        b_to_t_head: f64,
        a: f64,
    ) {
        print_info(l, i_j2, zerobody, one_plus_two, b_to_t_head, EM::E, a);
        println!();
        print_info(l, f_j2, zerobody, one_plus_two, b_to_t_head, EM::E, a);
    }
    pub fn magnetic(
        l: usize,
        i_j2: f64,
        f_j2: f64,
        zerobody: f64,
        one_plus_two: f64,
        b_to_t_head: f64,
        a: f64,
    ) {
        print_info(l, i_j2, zerobody, one_plus_two, b_to_t_head, EM::M, a);
        println!();
        print_info(l, f_j2, zerobody, one_plus_two, b_to_t_head, EM::M, a);
    }
    fn print_info(
        l: usize,
        j2: f64,
        zerobody: f64,
        one_plus_two: f64,
        b_to_t_head: f64,
        sigma: EM,
        a: f64,
    ) {
        let (sigma_str, unit, wu_factor) = match sigma {
            EM::E => ("E", format!("e^2 fm^{}", 2 * l), wu(a, l, sigma)),
            EM::M => ("M", format!("(μ_N/c)^2 fm^{}", 2 * l - 2), wu(a, l, sigma)),
        };
        println!("if J_initial  ={}", j2);
        let b_sigma_lambda = (zerobody + one_plus_two) * (zerobody + one_plus_two) / (j2 + 1.0);
        println!("    <B({}{})> = {} {}", sigma_str, l, b_sigma_lambda, unit);
        println!("            = {} W.u", b_sigma_lambda / wu_factor);
        println!(
            "    <T({}{})> = {} 1/s",
            sigma_str,
            l,
            b_to_t_head * b_sigma_lambda
        );
        println!(
            "    t_1/2 = {} second",
            2_f64.ln() / (b_to_t_head * b_sigma_lambda)
        );
    }
    pub fn get_tr_energy(i_num: u8, f_num: u8, transition_density_file: String) -> (f64, f64) {
        let nucleus = transition_density_file
            .split_once("log_")
            .unwrap()
            .1
            .split_once("_")
            .unwrap()
            .0;

        let a = get_az(nucleus).unwrap().0;

        let summary = transition_density_file
            .replace("log", "summary")
            .split_once("_tr_")
            .unwrap()
            .0
            .to_owned()
            + ".txt";
        let file = File::open(summary);
        let lines: Vec<String> =
            BufReader::new(file.expect("Wrong in open kshell summary file!!!"))
                .lines()
                .map(|line| line.unwrap().trim().to_string())
                .collect();
        let mut ei: f64 = 0.0;
        let mut is_set_ei: bool = false;
        let mut ef: f64 = 0.0;
        let mut is_set_ef: bool = false;
        for i in 5..lines.len() {
            if is_set_ei && is_set_ef {
                break;
            }
            let parts = lines[i].split_whitespace().collect::<Vec<&str>>();
            match parts[0].parse::<u8>().unwrap() {
                i if i == i_num => {
                    ei = parts[6].parse::<f64>().unwrap();
                    is_set_ei = true;
                }
                f if f == f_num => {
                    ef = parts[6].parse::<f64>().unwrap();
                    is_set_ef = true;
                }
                _ => continue,
            }
        }
        return ((ef - ei).abs(),a);
    }
    pub fn em_factor(e: f64, l: usize, sigma: EM) -> f64 {
        let const_part = f_lambda(l) * (e / HBARC).powf((2 * l + 1) as f64);
        match sigma {
            EM::E => ALPHA * 8.0 * PI * C * const_part,
            EM::M => {
                ALPHA * 8.0 * PI * C * HBARC * HBARC * const_part / (4.0 * M_PROTON * M_PROTON)
            }
        }
    }
    fn f_lambda(l: usize) -> f64 {
        match l {
            1 => 2.0 / 9.0,
            2 => 1.0 / 150.0,
            3 => 4.0 / 33075.0,
            4 => 1.0 / 714420.0,
            5 => 6.0 / 540280125.0,
            6 => 1.0 / 15652687050.0,
            _ => panic!(" Wrong ! Lambda should be 1,2,3,4,5,6 !!!"),
        }
    }
    fn wu(a: f64, l: usize, sigma: EM) -> f64 {
        let l2 = (2 * l) as f64;
        let lp3 = (l + 3) as f64;
        match sigma {
            EM::E => (1.2_f64).powf(l2) * 9.0 * a.powf(l2 / 3.0) / (4.0 * PI * lp3 * lp3),
            EM::M => 90.0 * (1.2_f64).powf(l2 - 2.0) * a.powf((l2 - 2.0) / 3.0) / (PI * lp3 * lp3),
        }
    }
}

mod redius {
    use crate::tool::get_nz;

    pub fn get_p_redius(
        i_j2: f64,
        f_j2: f64,
        zerobody: f64,
        one_plus_two: f64,
        transition_density_file: String,
    ) {
        let (n, z) = get_nz(
            &transition_density_file
                .split_once("log_")
                .unwrap()
                .1
                .split_once("_")
                .unwrap()
                .0,
        )
        .unwrap();
        let rc2_correction = 0.77 - 0.1149 * n / z + 0.033;
        println!("if J_initial = {}", i_j2);
        let rp2_i = zerobody + one_plus_two / (i_j2 + 1.0).sqrt();
        println!("    Proton redius <Rp> = {}", rp2_i.sqrt());
        let rc2_i = rp2_i + rc2_correction;
        println!("    Charge redius <Rc> = {}", rc2_i.sqrt());
        println!();
        println!("if J_initial = {}", f_j2);
        let rp2_f = zerobody + one_plus_two / (f_j2 + 1.0).sqrt();
        println!("    Proton redius <Rp> = {}", rp2_f.sqrt());
        let rc2_f = rp2_f + rc2_correction;
        println!("    Charge redius <Rc> = {}", rc2_f.sqrt());
    }
    pub fn get_n_redius(i_j2: f64, f_j2: f64, zerobody: f64, one_plus_two: f64) {
        println!("if J_initial = {}", i_j2);
        let rn2_i = zerobody + one_plus_two / (i_j2 + 1.0).sqrt();
        println!("    Neutron redius <Rn> = {}", rn2_i.sqrt());
        println!();
        println!("if J_initial = {}", f_j2);
        let rn2_f = zerobody + one_plus_two / (f_j2 + 1.0).sqrt();
        println!("    Neutron redius <Rn> = {}", rn2_f.sqrt());
    }
}
