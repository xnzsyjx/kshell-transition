use std::{
    cmp::Ordering,
    collections::HashMap,
    env,
    fs::File,
    io::{BufRead, BufReader},
    sync::Arc,
};

use crate::{expection::calculate_expection, operator::Operator, tool::extract_twoj2};

mod expection;
mod operator;
mod tool;

fn main() {
    let mut arg_map: HashMap<String, String> = HashMap::new();
    println!("------------- Parameter set -------------");
    for istr in env::args().into_iter().filter(|i| i.contains("=")) {
        let str_pair: Vec<String> = istr.split("=").map(|i| i.to_string()).collect();
        if str_pair.len() != 2 {
            println!("!!! Wrong input: {}", istr);
            return;
        }
        arg_map.insert(
            str_pair.first().unwrap().to_string(),
            str_pair.last().unwrap().to_string(),
        );
    }
    let op_file_head = arg_map
        .get("op_file_head")
        .expect("Wrong in head of operator file!");

    let op1b_file = &(op_file_head.to_owned() + "_1b.op");
    println!("op_1b_file     ===> {}", op1b_file);
    if File::open(op1b_file).is_err() {
        panic!("Trouble reading {} exiting.", op1b_file);
    }

    let op2b_file: &str = &(op_file_head.to_owned() + "_2b.op");
    println!("op_2b_file     ===> {}", op2b_file);
    if File::open(op2b_file).is_err() {
        panic!("Trouble reading {} exiting.", op2b_file);
    }
    let transition_density_file = arg_map
        .get("trfile")
        .expect("Wrong in transition density file!");
    println!("tr_file        ===> {}", transition_density_file);
    if File::open(transition_density_file).is_err() {
        panic!("Trouble reading {} exiting.", transition_density_file);
    }

    let i_num: u8 = arg_map
        .get("i_num")
        .expect("Wrong in num of initial state!")
        .parse::<u8>()
        .unwrap();
    println!("num of i state ===> {}", i_num);
    let f_num: u8 = arg_map
        .get("f_num")
        .expect("Wrong in num of final state!")
        .parse::<u8>()
        .unwrap();
    println!("num of f state ===> {}", f_num);
    println!();
    println!("Setting Operator ...");
    let op = Arc::new(Operator::new_op(op1b_file, op2b_file));
    println!("Operator has been set!\n");

    println!("Name of Operator: {}\n", op.get_name());

    let file = File::open(transition_density_file);
    let reader = BufReader::new(file.expect("Trouble reading transition density file exiting."));
    let i_string = &(i_num.to_string() + ")");
    let f_string = &(f_num.to_string() + ")");
    let mut line_iter = reader.lines();

    // part of OBTD
    let line_obtd_wf = loop {
        let line = line_iter
            .next()
            .unwrap_or_else(|| panic!("Something wrong with transition density file!\nThere is no line like: w.f. ..."))
            .unwrap();

        if line.contains("w.f.") && line.contains(i_string) && line.contains(f_string) {
            break line;
        } else {
            continue;
        }
    };

    let (i_j2, f_j2) = extract_twoj2(&line_obtd_wf);

    println!(
        "J1 = {} <=====> J2 = {}\n",
        i_j2 as f64 * 0.5,
        f_j2 as f64 * 0.5
    );

    println!("Zero-body: {}", op.get_zerobody());

    let line_obtd_num = line_iter
        .next()
        .unwrap_or_else(|| panic!("Something wrong with transition density file!"))
        .unwrap();

    let num_obtd_lines = if line_obtd_num.contains("OBTD") {
        line_obtd_num
            .split_whitespace()
            .last()
            .unwrap()
            .parse::<u32>()
            .unwrap()
    } else {
        panic!("Something wrong with transition density file!")
    };

    let mut one_expect = 0.0;
    for _ in 0..num_obtd_lines {
        let line = line_iter.next().unwrap().unwrap();

        let parts: Vec<&str> = line.split(":").collect();

        let td_rank_j = parts.get(2).unwrap().trim().parse::<u16>().unwrap();
        if op.get_rank_j() > td_rank_j {
            continue;
        } else if op.get_rank_j() < td_rank_j {
            break;
        }

        let (i_str, j_str) = parts.get(1).unwrap().trim().split_once(" ").unwrap();
        let (i, j) = (
            i_str.trim().parse::<u16>().unwrap(),
            j_str.trim().parse::<u16>().unwrap(),
        );
        one_expect += op.get_obme(i, j)
            * parts.last().unwrap().trim().parse::<f64>().unwrap()
            * if op.is_scalar() {
                (op.get_oi_j2(i) + 1.0).sqrt()
            } else {
                1.0
            };
    }
    println!("One-body:  {}", one_expect);

    // part of TBTD
    let _line_tbtd_wf = loop {
        let line = line_iter
                .next()
                .unwrap_or_else(|| panic!("Something wrong with transition density file!\nThere is no line like: w.f. ..."))
                .unwrap();

        if line.contains("w.f.") && line.contains(i_string) && line.contains(f_string) {
            if (i_j2, f_j2) != extract_twoj2(&line) {
                continue;
            } else {
                break line;
            }
        } else {
            continue;
        }
    };

    let line_tbtd_num = line_iter
        .next()
        .unwrap_or_else(|| panic!("Something wrong with transition density file!"))
        .unwrap();

    let num_tbtd_lines = if line_tbtd_num.contains("TBTD") {
        line_tbtd_num
            .split_whitespace()
            .last()
            .unwrap()
            .parse::<u32>()
            .unwrap()
    } else {
        panic!("Something wrong with transition density file!")
    };

    let mut two_expect = 0.0;
    for _ in 0..num_tbtd_lines {
        let line = line_iter.next().unwrap().unwrap();

        let parts: Vec<&str> = line.split(":").collect();

        let j_j_rank: Vec<u16> = parts
            .get(2)
            .unwrap()
            .split_whitespace()
            .map(|s| s.parse::<u16>().unwrap())
            .collect();
        let td_rank_j = *j_j_rank.last().unwrap();
        match op.get_rank_j().cmp(&td_rank_j) {
            Ordering::Less => break,
            Ordering::Equal => {
                let (j1, j2) = (*j_j_rank.get(0).unwrap(), *j_j_rank.get(1).unwrap());

                let i_j_k_l: Vec<&str> = parts.get(1).unwrap().split_whitespace().collect();

                let (i, j, k, l) = (
                    i_j_k_l.get(0).unwrap().parse::<u16>().unwrap(),
                    i_j_k_l.get(1).unwrap().parse::<u16>().unwrap(),
                    i_j_k_l.get(2).unwrap().parse::<u16>().unwrap(),
                    i_j_k_l.get(3).unwrap().parse::<u16>().unwrap(),
                );

                two_expect += op.get_tbme(j1, j2, i, j, k, l)
                    * parts.last().unwrap().trim().parse::<f64>().unwrap()
                    * if op.is_scalar() {
                        ((2 * j1 + 1) as f64).sqrt()
                    } else {
                        1.0
                    }
            }
            Ordering::Greater => continue,
        }
    }
    println!("Two-body:  {}", two_expect);

    // let obptb = one_expect + two_expect;
    println!();

    calculate_expection(
        i_num,
        i_j2 as f64,
        f_num,
        f_j2 as f64,
        op.get_name(),
        op.get_zerobody(),
        one_expect + two_expect,
        transition_density_file.to_string(),
    )
}
