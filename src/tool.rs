pub trait BasicForUInt {
    fn phase(self) -> f64;
}

impl BasicForUInt for u16 {
    fn phase(self) -> f64 {
        if self % 2 == 0 {
            1.0
        } else {
            -1.0
        }
    }
}

/**
 * example: "w.f.  J1= 17/2(   22)     J2= 13/2(   27)" or "w.f.  J1= 17/2(   22)     J2= 13/2(      27)"
 * return: (2*J1,2*J2), which is (17,13) in this example
 */
pub fn extract_twoj2(line: &str) -> (u16, u16) {
    let parts: Vec<&str> = line.split("/").collect();
    let parse_j = |i: usize| {
        parts
            .get(i)
            .and_then(|part| part.split_whitespace().last())
            .unwrap()
            .parse::<u16>()
            .ok()
    };
    match (parse_j(0), parse_j(1)) {
        (Some(j1_2), Some(j2_2)) => (j1_2, j2_2),
        _ => panic!("Wrong in w.f line!!!"),
    }
}

/// accept formats: Na22, return Ok((11,11))
pub fn get_nz(s: &str) -> Result<(f64, f64), String> {
    let mut i: usize = 0;
    while !s.as_bytes()[i].is_ascii_digit() {
        i += 1;
    }
    let elem: &str = &s[..i];
    let a: usize = s[i..].parse::<usize>().unwrap();
    if a > 400 {
        return Err(format!("AAAHHH !!! Nuclear Mass A = {} is so big", a));
    }
    let z: usize = constant::PERIODIC_TABLE
        .iter()
        .position(|&e| e == elem)
        .ok_or_else(|| format!("Trouble parsing Z via {}", s))?;
    if z > a || z > 150 {
        return Err(format!("AAAHHH !!! Trouble Getting Z via {} !!!", s));
    }
    Ok(((a - z) as f64, z as f64))
}

/// accept formats: Na22, return Ok((22,11))
pub fn get_az(s: &str) -> Result<(f64, f64), String> {
    let mut i: usize = 0;
    while !s.as_bytes()[i].is_ascii_digit() {
        i += 1;
    }
    let elem: &str = &s[..i];
    let a: usize = s[i..].parse::<usize>().unwrap();
    if a > 400 {
        return Err(format!("AAAHHH !!! Nuclear Mass A = {} is so big", a));
    }
    let z: usize = constant::PERIODIC_TABLE
        .iter()
        .position(|&e| e == elem)
        .ok_or_else(|| format!("Trouble parsing Z via {}", s))?;
    if z > a || z > 150 {
        return Err(format!("AAAHHH !!! Trouble Getting Z via {} !!!", s));
    }
    Ok((a as f64, z as f64))
}

pub mod constant {
    pub const PI: f64 = 3.141592653589793238462643383279502884197;
    pub const ALPHA: f64 = 7.2973525693e-3; // Fine-structure pub constant
    pub const C: f64 = 2.99792458e23; // velocity of light, fm/s
    pub const HBARC: f64 = 197.3269718; // hbar*c in MeV * fm
    pub const M_PROTON: f64 = 938.2720813; // mass of proton in MeV/c^2
    pub const PERIODIC_TABLE: [&str; 119] = [
        "n", "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al", "Si", "P",
        "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn",
        "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh",
        "Pd", "Ag", "Cd", "In", "Sn", "Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd",
        "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu", "Hf", "Ta", "W", "Re",
        "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn", "Fr", "Ra", "Ac", "Th",
        "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No", "Lr", "Rf", "Db",
        "Sg", "Bh", "Hs", "Mt", "Ds", "Rg", "Cn", "Nh", "Fl", "Mc", "Lv", "Ts", "Og",
    ];
}
