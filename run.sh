#!/bin/bash

i_num=1
f_num=14

op_file_path=/home_data/fsq/kshell-master/fe53-converge/hw14-Emax14
op_file_head=${op_file_path}/EM1.8-2.0_oslo_atan_HF_N14_hw14_core-Ca40_Fe53_E6

transition_file_path=/home_data/fsq/kshell-master/fe53-converge/hw14-Emax14
transition_file=${transition_file_path}/log_Fe53_EM1.8-2.0_atan_HF_N14_hw14_core-Ca40_Fe53_kshell_tr_m1n_m1n.txt

cargo run --release i_num=${i_num} f_num=${f_num} op_file_head=${op_file_head} trfile=${transition_file}